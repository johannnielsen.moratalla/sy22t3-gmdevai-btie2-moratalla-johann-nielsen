#include <iostream>
#include <ctime>
#include <cstdlib>

class Wizard
{
private:
    int hp;
    int mp;

public:
    Wizard() :hp(250), mp(0) {}

    int getHP() const { return hp; }
    int getMP() const { return mp; }

    bool isAlive() const { return hp > 0; }

    void attack(Wizard& other)
    {
        int damage = rand() % 6 + 10;
        std::cout << "Wizard attacks for " << damage << " damage!" << std::endl;
        other.takeDamage(damage);
        mp += rand() % 11 + 10;
    }

    void castSpell(Wizard& other)
    {
        if (mp >= 50)
        {
            int damage = rand() % 21 + 40;
            std::cout << "Wizard casts a spell for " << damage << " damage!" << std::endl;

            other.takeDamage(damage);
            mp -= 50;
        }
        else
        {
            std::cout << "Not enough MP to cast the spell!" << std::endl;

            attack(other);
        }
    }

    void takeDamage(int damage)
    {
        hp -= damage;

        if (hp < 0)
            hp = 0;
    }
};

int main()
{
    srand(static_cast<unsigned int>(time(0)));

    Wizard wizard1, wizard2;

    while (wizard1.isAlive() && wizard2.isAlive())
    {
        std::cout << "Wizard 1's turn: " << std::endl;

        if (wizard1.getMP() >= 50)
        {
            wizard1.castSpell(wizard2);
        }
        else
        {
            wizard1.attack(wizard2);
        }

        std::cout << "Wizard 1 HP: " << wizard1.getHP() << ", MP: " << wizard1.getMP() << std::endl;
        std::cout << "Wizard 2 HP: " << wizard2.getHP() << ", MP: " << wizard2.getMP() << std::endl;
        std::cout << "--------------------------------" << std::endl;

        if (!wizard2.isAlive())
        {
            break;
        }
        std::cout << "Wizard 2's turn: " << std::endl;

        if (wizard2.getMP() >= 50)
        {
            wizard2.castSpell(wizard1);
        }
        else
        {
            wizard2.attack(wizard1);
        }

        std::cout << "Wizard 1 HP: " << wizard1.getHP() << ", MP: " << wizard1.getMP() << std::endl;
        std::cout << "Wizard 2 HP: " << wizard2.getHP() << ", MP: " << wizard2.getMP() << std::endl;
        std::cout << "--------------------------------" << std::endl;
    }

    if (wizard1.isAlive())
    {
        std::cout << "Wizard 1 wins!" << std::endl;
    }
    else if (wizard2.isAlive())
    {
        std::cout << "Wizard 2 wins!" << std::endl;
    }
    else
    {
        std::cout << "It's a tie!" << std::endl;
    }

    return 0;
}