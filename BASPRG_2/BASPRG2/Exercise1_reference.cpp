#include <iostream>
#include <string>
#include <random>

const std::string PLAYER_NAME = "Player 1";
const int STARTING_GOLD = 1000;
const int MIN_BET = 1;

class Dice
{
private:
    std::random_device rd;
    std::mt19937 gen;

public:
    Dice() : gen(rd()) 
    {

    }
    void RollTwoDice(int& dice1, int& dice2) 
    {
        std::uniform_int_distribution<> dis(1, 6);
        dice1 = dis(gen);
        dice2 = dis(gen);
    }

    
};

class Player
{
public:
    std::string Name;
    int Gold;

    Player(const std::string& name, int startingGold): Name(name), Gold(startingGold) 
    {

    }
};

class Game
{
private:
    Dice dice;
    Player player;
    int aiGold;

public:
    Game(const std::string& playerName, int startingGold):dice(), player(playerName, startingGold), aiGold(startingGold) 
    {

    }

    void PlayRound()
    {
        std::cout << "Current Gold: " << player.Gold << std::endl;
        int bet = GetValidBetAndDeductGold();

        int aiDice1 = 0, aiDice2 = 0;
        int playerDice1 = 0, playerDice2 = 0;

        dice.RollTwoDice(aiDice1, aiDice2);
        dice.RollTwoDice(playerDice1, playerDice2);

        int aiRoll = aiDice1 + aiDice2;
        int playerRoll = playerDice1 + playerDice2;

        std::cout << "AI rolled: " << aiDice1 << " + " << aiDice2 << " = " << aiRoll << std::endl;
        std::cout << player.Name << " rolled: " << playerDice1 << " + " << playerDice2 << " = " << playerRoll << std::endl;

        EvaluateOutcome(bet, aiRoll, playerRoll, playerDice1, playerDice2);

        std::cout << std::endl;
    }

    void EvaluateOutcome(int bet, int aiRoll, int playerRoll, int playerDice1, int playerDice2)
    {
        if (playerRoll > aiRoll)
        {
            player.Gold += bet;
            aiGold -= bet;
            std::cout << player.Name << " wins!" << std::endl;
        }
        else if (playerRoll == 2 && playerDice1 == 1 && playerDice2 == 1)
        {
            player.Gold += bet * 2;
            aiGold -= bet * 2;
            std::cout << player.Name << " wins with Snake Eyes!" << std::endl;
        }
        else if (playerRoll == aiRoll)
        {
            std::cout << "It's a draw!" << std::endl;
        }
        else
        {
            player.Gold -= bet;
            aiGold += bet;
            std::cout << player.Name << " loses!" << std::endl;
        }
    }

    void StartGame()
    {
        while (player.Gold > 0)
        {
            PlayRound();
        }

        std::cout << "Game Over. You've run out of gold." << std::endl;
    }

    int GetValidBetAndDeductGold()
    {
        int bet = 0;
        bool isValidBet = false;

        while (!isValidBet)
        {
            std::cout << "Enter your bet: ";
            std::cin >> bet;

            if (!std::cin || bet < MIN_BET || bet > player.Gold)
            {
                std::cout << "Invalid bet. Enter a valid amount." << std::endl;
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
            else
            {
                isValidBet = true;
            }
        }
        return bet;
    }
};

int main()
{
    std::string playerName = PLAYER_NAME;
    int startingGold = STARTING_GOLD;
    Game game(playerName, startingGold);

    std::cout << "Welcome to the Dice Game!" << std::endl;

    game.StartGame();

    return 0;
}