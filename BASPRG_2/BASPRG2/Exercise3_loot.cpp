#include <iostream>
#include <cstdlib>
#include <ctime>

struct Loot
{
    std::string name;
    int gold;
};

int getRandomLootValue(int multiplier)
{
    static const Loot lootTable[] =
    {
        { "Mithril Ore", 100 },
        { "Sharp Talon", 50 },
        { "Thick Leather", 25 },
        { "Jellopy", 5 },
        { "Cursed Stone", 0 }
    };

    int lootType = rand() % 5; // Randomly select a loot 
    const Loot& randomLoot = lootTable[lootType];

    return randomLoot.gold * multiplier;
}

int main()
{
    const int targetGold = 500;
    const int enterCost = 25;
    const int initialGold = 50;

    int currentGold = initialGold;
    int lootCostMultiplier = 1;

    std::cout << "Welcome to the Dungeon Game!\n";
    std::cout << "You have entered a dungeon.\n";

    srand(static_cast<unsigned int>(time(0)));

    while (currentGold >= enterCost && currentGold < targetGold)
    {
        std::cout << "Current Gold: " << currentGold << "\n";
        std::cout << "Entering the dungeon costs " << enterCost << " gold.\n";

        std::cout << "Do you want to enter the dungeon? (y/n): ";
        std::string choice;
        std::cin >> choice;

        if (choice == "n" || choice == "N")
        {
            std::cout << "You have chosen to exit the dungeon.\n";
            break;
        }

        if (choice == "y" || choice == "Y")
        {
            currentGold -= enterCost;
            int lootValue = getRandomLootValue(lootCostMultiplier);

            static const Loot lootTable[] =
            {
                { "Mithril Ore", 100 },
                { "Sharp Talon", 50 },
                { "Thick Leather", 25 },
                { "Jellopy", 5 },
                { "Cursed Stone", 0 }
            };

            std::string lootName;
            for (const Loot& loot : lootTable)
            {
                if (loot.gold == lootValue)
                {
                    lootName = loot.name;
                    break;
                }
            }

            std::cout << "You found a " << lootName << "!" << " worth " << lootValue << " gold.\n";

            if (lootValue == 0)
            {
                std::cout << "You found a Cursed Stone and DIED!\n";
                currentGold = 0;
                break;
            }

            currentGold += lootValue;
            lootCostMultiplier++;
        }
        else
        {
            std::cout << "Invalid input. Please enter 'y' or 'n'.\n";
        }
    }

    if (currentGold >= targetGold)
    {
        std::cout << "Congratulations! You have reached the target gold and won the game!\n";
    }
    else if (currentGold < enterCost)
    {
        std::cout << "Game over! You don't have enough gold to enter/continue the dungeon.\n";
    }

    std::cout << "Thank you for playing!\n";

    return 0;
}