#include <iostream>
#include <ctime>
#include <cstdlib>
#include <string>
#include <vector>
#include <algorithm>

struct Node 
{
    std::string name;
    Node* next;
};

class NightWatch 
{
private:
    Node* head;

public:
    NightWatch() 
    {
        head = nullptr;
    }

    ~NightWatch() 
    {
        
        Node* current = head;
        if (current != nullptr) 
        {
            Node* temp = current->next;
            while (temp != head) 
            {
                delete current;
                current = temp;
                temp = temp->next;
            }
            delete current;  
        }
    }

    void addMember(const std::string& name) 
    {
        Node* newNode = new Node;
        newNode->name = name;

        if (head == nullptr) 
        {
            head = newNode;
        }

        newNode->next = head;
        Node* last = head;

        while (last->next != head) 
        {
            last = last->next;
        }
        last->next = newNode;
    }

    void eliminateRandomMember() 
    {
        if (head == nullptr) 
        {
            std::cout << "No members in the Night's Watch." << std::endl;
            return;
        }

        if (head->next == head) 
        {
            std::cout << head->name << " is the only remaining member." << std::endl;
            std::cout << head->name << " will go to seek reinforcements." << std::endl;

            delete head;
            head = nullptr;

            return;
        }

        std::vector<Node*> remainingMembers;
        Node* current = head;
        do 
        {
            remainingMembers.push_back(current);
            current = current->next;
        } while (current != head);

        std::random_shuffle(remainingMembers.begin(), remainingMembers.end());
        Node* eliminatedMember = remainingMembers.back();

        for (auto it = remainingMembers.begin(); it != remainingMembers.end(); ++it) 
        {
            if (*it == eliminatedMember) 
            {
                remainingMembers.erase(it);
                break;
            }
        }

        head = remainingMembers.front();
        current = head;
        for (auto it = remainingMembers.begin() + 1; it != remainingMembers.end(); ++it) 
        {
            current->next = *it;
            current = current->next;
        }
        current->next = head;

        std::random_shuffle(remainingMembers.begin(), remainingMembers.end());
        Node* pickedPlayer = remainingMembers.front();

        std::cout << current->name << " picks " << eliminatedMember->name << " out of the group." << std::endl;
        std::cout << eliminatedMember->name << " was eliminated" << std::endl;

        delete eliminatedMember;
    }

    std::string getRemainingMember() 
    {
        if (head == nullptr) 
        {
            std::cout << "No members in the Night's Watch." << std::endl;
            return "";
        }

        return head->name;
    }

    Node* getHead() 
    {
        return head;
    }
};

int main() 
{
    std::srand(static_cast<unsigned int>(std::time(nullptr)));
    NightWatch nightWatch;

    // Add members to the Night's Watch
    int numMembers;
    do 
    {
        std::cout << "How many players are there? ";
        std::cin >> numMembers;

        if (numMembers <= 0)
        {
            std::cout << "Invalid number. Please enter a positive number." << std::endl;
        }
    } while (numMembers <= 0);

    for (int i = 0; i < numMembers; i++) 
    {
        std::string name;
        std::cout << "What's your name, soldier? ";
        std::cin >> name;

        nightWatch.addMember(name);
    }

    std::cout << "==========================================\n";
    std::cout << "ROUND 1\n";
    std::cout << "==========================================\n";

    int round = 1;
    while (true) 
    {
        std::cout << "Remaining Members:\n";
        Node* current = nightWatch.getHead();
        do 
        {
            std::cout << current->name << std::endl;
            current = current->next;

        } while (current != nightWatch.getHead());

        int numRemainingMembers = numMembers - (round - 1);
        if (numRemainingMembers == 1) 
        {
            break;  
        }

        std::cout << "Result: \n" << std::endl;
        nightWatch.eliminateRandomMember();

        std::cout << "==========================================\n";
        std::cout << "ROUND " << round + 1 << "\n";
        std::cout << "==========================================\n";

        round++;
    }

    std::cout << "==========================================\n";
    std::cout << "FINAL RESULT\n";
    std::cout << "==========================================\n";
    std::string remainingMember = nightWatch.getRemainingMember();

    if (!remainingMember.empty()) 
    {
        std::cout << remainingMember << " will go to seek reinforcements." << std::endl;
    }

    return 0;
}