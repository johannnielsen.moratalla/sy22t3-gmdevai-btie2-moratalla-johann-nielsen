#include "Assassin.h"

Assassin::Assassin(int power, int vit, int agi, int dex)
{
	mPower = power;
	mVitality = vit;
	mAgility = agi;
	mDexterity = dex;
}

int Assassin::getPower()
{
	return mPower;
}

int Assassin::getVit()
{
	return mVitality;
}

int Assassin::getAgi()
{
	return mAgility;
}

int Assassin::getDex()
{
	return mDexterity;
}
