#pragma once
class Assassin
{
public:
	Assassin(int power, int vit, int agi, int dex);

	int getPower();
	int getVit();
	int getAgi();
	int getDex();

private:

	int mPower;
	int mVitality;
	int mAgility;
	int mDexterity;
};

