#include "Character.h"
#include "Warrior.h"
#include "Mage.h"
#include "Assassin.h"

Character::Character()
{
	mCurrentHp = rand() % (30 - 20 + 1) + 20;
	mMaxHp = mCurrentHp;
	mPower = rand() % (15 - 5 + 1) + 5;
	mVitality = rand() % (10 - 1 + 1) + 1;
	mAgility = rand() % (10 - 1 + 1) + 1;
	mDexterity = rand() % (10 - 1 + 1) + 1;
}

Character::~Character()
{
	delete mWarrior;
	delete mAssassin;
	delete mMage;
}

void Character::attack(Character* target)
{
	int damage = (this->mPower - target->mVitality);
	float bonusDamage = 1.5f;

	target->getmClass() = "Assassin";

	if (getmClass() == "Warrior")
	{
		if (target->getmClass() == "Assassin")
		{
			damage *= bonusDamage;
			cout << getName() << " is stronger than " << target->getName() << "'s class,  + 50% bonus Damage!" << endl;
		}
	}
	else if (getmClass() == "Assassin")
	{
		if (target->getmClass() == "Mage")
		{
			damage *= bonusDamage;
			cout << getName() << " is stronger than " << target->getName() << "'s class,  + 50% bonus Damage!" << endl;
		}
	}
	else if (getmClass() == "Mage")
	{
		if (target->getmClass() == "Warrior")
		{
			damage *= bonusDamage;
			cout << getName() << " is stronger than " << target->getName() << "'s class, + 50% bonus Damage!" << endl;
		}
	}

	if (mPower <= target->mVitality)
	{
		damage = 1;
	}

	if (getHitRate() < 30)
	{
		damage = 0;
	}


	target->mCurrentHp -= damage;

	cout << mName << " dealt " << damage << " Dmg to " << target->mName << endl;
	if (damage == 0)
	{
		cout << "Since the attack missed!" << endl;
	}

}

void Character::print()
{
	cout << "Name: " << mName << endl;
	cout << "Chosen Class: " << mClass << endl;
	cout << "Hp: " << mCurrentHp << "/ " << mMaxHp << endl;
	cout << "Power: " << mPower << endl;
	cout << "Vitality: " << mVitality << endl;
	cout << "Agility: " << mAgility << endl;
	cout << "Dexteririty: " << mDexterity << endl;
}


string Character::getName()
{
	return mName;
}

int Character::getHp()
{
	return mCurrentHp;
}

int Character::getPower()
{
	return mPower;
}

float Character::getVit()
{
	return mVitality;
}

float Character::getAgi()
{
	return mAgility;
}

int Character::getDex()
{
	return mDexterity;
}

string Character::getRandClass()
{
	return mRandClass;
}

string Character::getmClass()
{
	return mClass;
}

int Character::getHitRate()
{
	return mHitRate;
}

void Character::setHp()
{

	if (mCurrentHp < 1)
	{
		mCurrentHp = 0;
	}
	if (mCurrentHp > mMaxHp)
	{
		mCurrentHp = mMaxHp;
	}
}

void Character::setName(string name)
{
	mName = name;
}

void Character::inputName()
{
	string name;

	cout << "What is your player's name?" << endl;
	cin >> name;

	mName = name;
	cout << "Name: " << mName << endl;
}

void Character::chooseClass()
{
	int chosenClass;

	cout << "Which type do you want to choose?" << endl;
	cout << "[1] Warrior" << endl;
	cout << "[2] Assasin" << endl;
	cout << "[3] Mage" << endl;

	cin >> chosenClass;

	mChooseClass = chosenClass;

	if (chosenClass == 1)
	{
		mClass = "Warrior";
	}
	else if (chosenClass == 2)
	{
		mClass = "Assassin";
	}
	else if (chosenClass == 3)
	{
		mClass = "Mage";
	}

}

void Character::setStats(Character* target)
{

	if (target->getmClass() == "Warrior")
	{
		Warrior* mWarrior = new Warrior(3, 3, 0, 0);
		cout << "+" << mWarrior->getPower() << " Power" << endl;
		cout << "+" << mWarrior->getVit() << " Vitality" << endl;
		cout << "+" << mWarrior->getAgi() << " Agility" << endl;
		cout << "+" << mWarrior->getDex() << " Dexterity" << endl;

		mPower += mWarrior->getPower();
		mVitality += mWarrior->getVit();
		mAgility += mWarrior->getAgi();
		mDexterity += mWarrior->getDex();
	}
	else if (target->getmClass() == "Assassin")
	{
		Assassin* mAssassin = new Assassin(0, 0, 3, 3);
		cout << "+" << mAssassin->getPower() << " Power" << endl;
		cout << "+" << mAssassin->getVit() << " Vitality" << endl;
		cout << "+" << mAssassin->getAgi() << " Agility" << endl;
		cout << "+" << mAssassin->getDex() << " Dexterity" << endl;

		mPower += mAssassin->getPower();
		mVitality += mAssassin->getVit();
		mAgility += mAssassin->getAgi();
		mDexterity += mAssassin->getDex();
	}
	else if (target->getmClass() == "Mage")
	{
		Mage* mMage = new Mage(5, 0, 0, 0);
		cout << "+" << mMage->getPower() << " Power" << endl;
		cout << "+" << mMage->getVit() << " Vitality" << endl;
		cout << "+" << mMage->getAgi() << " Agility" << endl;
		cout << "+" << mMage->getDex() << " Dexterity" << endl;

		mPower += mMage->getPower();
		mVitality += mMage->getVit();
		mAgility += mMage->getAgi();
		mDexterity += mMage->getDex();
	}

}

void Character::setEnemyStats()
{
	mCurrentHp = rand() % (15 - 5 + 1) + 5;
	mMaxHp = mCurrentHp;
	mPower = rand() % (15 - 1 + 1) + 1;
	mVitality = rand() % (10 - 1 + 1) + 1;
	mAgility = rand() % (10 - 1 + 1) + 1;
	mDexterity = rand() % (10 - 1 + 1) + 1;
}

void Character::setRandomClass()
{
	int random = rand() % 3 + 1;

	if (random == 1)
	{

		mRandClass = "Warrior";
	}
	else if (random == 2)
	{

		mRandClass = "Assassin";
	}
	else if (random == 3)
	{
		mRandClass = "Mage";
	}

	mClass = mRandClass;
}

void Character::addEnemyStats()
{
	int value = 3;
	value += 3;
	int minValue = 2;
	minValue += 2;

	mMaxHp += rand() % ((value - minValue + 1) + minValue);
	mCurrentHp = mMaxHp;
	mPower += rand() % (value - minValue + 1) + minValue;
	mVitality += rand() % (value - minValue + 1) + minValue;
	mAgility += rand() % (value - minValue + 1) + minValue;
	mDexterity += rand() % (value - minValue + 1) + minValue;
}

void Character::setHitRate(Character* target)
{
	int hitPercent = (this->getDex() / target->getAgi()) * 100;

	if (hitPercent > 80)
	{
		hitPercent = 80;
	}
	else if (hitPercent < 20)
	{
		hitPercent = 20;
	}

	int randValue = rand() % (hitPercent - 20 + 1) + 20;
	mHitRate = randValue;


}

void Character::heal()
{
	int hpPercent = mMaxHp * .30;

	mCurrentHp += hpPercent;
	cout << hpPercent << " has been added to current health!" << endl;
}