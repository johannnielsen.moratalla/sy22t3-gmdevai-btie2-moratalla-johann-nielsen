#pragma once
#include <string>
#include <iostream>


using namespace std;

class Warrior;
class Mage;
class Assassin;

class Character
{
public:
	Character();
	~Character();

	void attack(Character* target);
	void print();

	string getName();
	int getHp();
	int getPower();
	float getVit();
	float getAgi();
	int getDex();
	string getRandClass();
	string getmClass();
	int getHitRate();

	void setHp();
	void setName(string name);
	void inputName();
	void chooseClass();
	void setStats(Character* target);
	void setEnemyStats();
	void setRandomClass();
	void addEnemyStats();
	void setHitRate(Character* target);
	void heal();


private:

	string mName;
	int mCurrentHp;
	int mMaxHp;
	int mPower;
	int mVitality;
	float mAgility;
	float mDexterity;

	int mChooseClass;
	int mHitRate;
	string mRandClass;
	string mClass;
	Warrior* mWarrior;
	Mage* mMage;
	Assassin* mAssassin;
};

#pragma once
