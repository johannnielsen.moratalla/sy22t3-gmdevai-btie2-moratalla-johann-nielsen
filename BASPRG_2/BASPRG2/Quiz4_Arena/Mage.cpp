#include "Mage.h"

Mage::Mage(int power, int vit, int agi, int dex)
{
	mPower = power;
	mVitality = vit;
	mAgility = agi;
	mDexterity = dex;
}

int Mage::getPower()
{
	return mPower;
}

int Mage::getVit()
{
	return mVitality;
}

int Mage::getAgi()
{
	return mAgility;
}

int Mage::getDex()
{
	return mDexterity;
}