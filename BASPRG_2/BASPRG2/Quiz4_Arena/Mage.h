#pragma once
#include <iostream>
#include <string>


using namespace std;

class Mage
{
public:
	Mage(int power, int vit, int agi, int dex);

	int getPower();
	int getVit();
	int getAgi();
	int getDex();

private:

	int mPower;
	int mVitality;
	int mAgility;
	int mDexterity;
};

