#include <iostream>
#include <string>
#include <time.h>
#include "Character.h"


using namespace std;

int main()
{
	srand(time(0));
	int round = 1;

	Character* player = new Character();
	player->inputName();
	player->chooseClass();

	Character* enemy = new Character();
	enemy->setName("Enemy");
	enemy->setEnemyStats();

	system("pause");
	system("cls");

	while (player->getHp() != 0)
	{
		cout << "Stage: " << round << endl;
		player->print();
		cout << endl;
		cout << "Vs" << endl;
		cout << endl;
		enemy->setRandomClass();
		enemy->print();

		system("pause");
		system("cls");

		cout << "===========================" << endl;

		while (enemy->getHp() != 0 && player->getHp() != 0)
		{
			if (player->getAgi() >= enemy->getAgi())
			{
				player->setHitRate(enemy);
				player->attack(enemy);
				enemy->setHp();
				cout << enemy->getName() << "'s HP: " << enemy->getHp() << endl;
				system("pause");

				if (enemy->getHp() <= 0)
				{
					break;
				}

				enemy->setHitRate(player);
				enemy->attack(player);
				player->setHp();
				cout << player->getName() << "'s HP: " << player->getHp() << endl;
				system("pause");
				cout << "===========================" << endl;
			}
			else if (enemy->getAgi() > player->getAgi())
			{
				enemy->setHitRate(player);
				enemy->attack(player);
				player->setHp();
				cout << player->getName() << "'s HP: " << player->getHp() << endl;
				system("pause");

				if (player->getHp() <= 0)
				{
					break;
				}
				player->setHitRate(enemy);
				player->attack(enemy);
				enemy->setHp();
				cout << enemy->getName() << "'s HP: " << enemy->getHp() << endl;
				system("pause");
				cout << "===========================" << endl;
			}

		}

		system("cls");

		cout << "Combat Results!" << endl;
		cout << "============================" << endl;
		player->print();
		if (player->getHp() != 0)
		{
			player->setStats(enemy);
			player->heal();
			player->setHp();
		}
		cout << "----------------------------" << endl;

		enemy->print();
		cout << "============================" << endl;
		system("pause");
		system("cls");

		enemy->setEnemyStats();
		enemy->addEnemyStats();
		enemy->setRandomClass();
		round++;
	}

	if (player->getHp() == 0)
	{
		cout << "You lost!" << endl;
		cout << "Stage reached: " << round << endl;
		cout << "Player stats:" << endl;
		player->print();
	}
	
	delete player;
	delete enemy;

	system("pause");
	return 0;
}