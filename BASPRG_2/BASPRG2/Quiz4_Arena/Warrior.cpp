#include "Warrior.h"

Warrior::Warrior(int power, int vit, int agi, int dex)
{
	mPower = power;
	mVitality = vit;
	mAgility = agi;
	mDexterity = dex;
}

int Warrior::getPower()
{
	return mPower;
}

int Warrior::getVit()
{
	return mVitality;
}

int Warrior::getAgi()
{
	return mAgility;
}

int Warrior::getDex()
{
	return mDexterity;
}
