#pragma once
#include <string>
#include <iostream>

using namespace std;

class Warrior
{
public:
	Warrior(int power, int vit, int agi, int dex);

	int getPower();
	int getVit();
	int getAgi();
	int getDex();

private:

	int mPower;
	int mVitality;
	int mAgility;
	int mDexterity;
};

