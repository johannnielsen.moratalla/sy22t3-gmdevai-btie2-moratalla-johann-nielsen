#pragma once
#include <iostream>
#include <string>

using namespace std;

class Unit;

class Action
{
public:
	Action(string name, int mp);

	string getName();
	int getMp();

	void setMp(int value);

	virtual void activate(Unit* attacker, Unit* target) = 0;


private:

	string mName;
	int mMp;
};

