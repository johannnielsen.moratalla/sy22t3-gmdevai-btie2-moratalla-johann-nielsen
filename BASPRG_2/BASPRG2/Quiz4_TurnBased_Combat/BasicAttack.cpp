#include "BasicAttack.h"
#include "Unit.h"

BasicAttack::BasicAttack()
	:Action("Basic Attack", 0)
{
}

void BasicAttack::activate(Unit* attacker, Unit* target)
{
	int damage = (attacker->getPow() - target->getVit());
	float bonusDamage = 1.0f;





	cout << getName() << endl;
	if (attacker->getPow() <= target->getVit())
	{
		damage = 4;
	}
	attacker->setHitRate(attacker, target);

	if (attacker->getHitRate() < 30)
	{

		damage = 0;
		cout << "Attack missed!" << endl;
	}
	else if (attacker->getHitRate() >= 30)

	{
		int critChance = rand() % 100 + 1;
		if (critChance <= 20)
		{
			damage *= .20;
			cout << "Critical!" << endl;
		}
		if (damage == 0)
		{
			damage = 4;
		}

		target->subtractHp(damage);

		cout << attacker->getName() << " dealt " << damage << " dmg to " << target->getName() << endl;

	}
}

