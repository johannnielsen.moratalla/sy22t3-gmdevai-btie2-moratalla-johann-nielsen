#pragma once
#include "Action.h"
#include <iostream>

using namespace std;

class Unit;

class BasicAttack :
    public Action
{
public:
    BasicAttack();

    void activate(Unit* attacker, Unit* target) override;
};

#pragma once
