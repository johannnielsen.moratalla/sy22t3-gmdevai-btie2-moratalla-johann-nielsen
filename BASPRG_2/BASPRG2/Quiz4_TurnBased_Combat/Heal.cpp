#include "Heal.h"
#include "Unit.h"

Heal::Heal()
	:Action("Heal", 3)
{
}

void Heal::activate(Unit* attacker, Unit* target)
{

	int heal = target->getMaxHp() * .30;



	target->addHp(heal);
	cout << attacker->getName() << " added " << heal << " hp to " << target->getName() << endl;

	target->setMp();
	attacker->subtractMp(3);
	cout << attacker->getMp() << "/ " << attacker->getMaxMp() << " Mp left" << endl;

}
