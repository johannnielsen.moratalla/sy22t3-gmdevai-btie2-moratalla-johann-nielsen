#pragma once
#include "Action.h"
#include <iostream>

using namespace std;

class Unit;

class Heal :
    public Action
{
public:
    Heal();

    void activate(Unit* attacker, Unit* target) override;
};

