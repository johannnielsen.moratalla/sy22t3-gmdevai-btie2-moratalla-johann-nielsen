#include "MultiTargetAttack.h"
#include "Unit.h"

MultiTargetAttack::MultiTargetAttack()
	:Action("Multi Target Attack", 4)
{
}

void MultiTargetAttack::activate(Unit* attacker, Unit* target)
{
	float bonusDamage = 0.9f;
	int damage = (attacker->getPow() - target->getVit());

	damage *= bonusDamage;


	if (attacker->getPow() <= target->getVit())
	{
		damage = 4;
	}
	else if (damage == 0)
	{
		damage = 4;
	}


	target->subtractHp(damage);
	cout << attacker->getName() << " dealt " << damage << " dmg to " << target->getName() << endl;



}
