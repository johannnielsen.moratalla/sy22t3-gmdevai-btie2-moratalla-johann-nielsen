#pragma once
#include "Action.h"
#include "iostream"

using namespace std;

class MultiTargetAttack :
    public Action
{
public:
    MultiTargetAttack();

    void activate(Unit* attacker, Unit* target) override;
};

