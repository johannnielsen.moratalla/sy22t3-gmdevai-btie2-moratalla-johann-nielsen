#include "SingleTargetAttack.h"
#include "Unit.h"

SingleTargetAttack::SingleTargetAttack()
	:Action("Single Target Attack", 5)
{

}

void SingleTargetAttack::activate(Unit* attacker, Unit* target)
{
	float bonusDamage = 2.2f;
	int damage = (attacker->getPow() - target->getVit());

	damage *= bonusDamage;


	if (attacker->getPow() <= target->getVit())
	{
		damage = 4;
	}

	target->subtractHp(damage);
	target->setMp();
	cout << attacker->getName() << " dealt " << damage << " dmg to " << target->getName() << endl;
	attacker->subtractMp(5);

	cout << attacker->getMp() << "/ " << attacker->getMaxMp() << " Mp left" << endl;

}
