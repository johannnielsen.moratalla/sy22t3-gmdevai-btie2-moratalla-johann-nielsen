#pragma once
#include "Action.h"
#include <iostream>

using namespace std;

class Unit;

class SingleTargetAttack :
    public Action
{
public:
    SingleTargetAttack();

    void activate(Unit* attacker, Unit* target) override;

};

