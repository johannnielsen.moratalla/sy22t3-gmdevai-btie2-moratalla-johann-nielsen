#include <iostream>
#include <string>
#include <time.h>
#include <vector>
#include <algorithm>
#include "Unit.h"
#include "Action.h"
#include "BasicAttack.h"
#include "SingleTargetAttack.h"
#include "MultiTargetAttack.h"
#include "Heal.h"

using namespace std;

Unit* unit = new Unit();


void printTeams()
{
	unit->printWarcraft();
	cout << "==========================" << endl;
	cout << "========== vs. ===========" << endl;
	cout << "==========================" << endl;

	unit->printDiablo();

	cout << "==========================" << endl;
}

void turnRemove()
{
	for (int i = 0; i < unit->getTurnOrder().size(); i++)
	{
		if (unit->getTurnOrder()[i]->getHp() <= 0)
		{
			unit->getTurnOrder().erase(unit->getTurnOrder().begin() + i);
		}

	}
}

void unitCheck()
{

	for (int i = 0; i < unit->diablo().size(); i++)
	{
		if (unit->diablo()[i]->getHp() <= 0)
		{
			cout << unit->diablo()[i]->getName() << " died" << endl;
			unit->diablo().erase(unit->diablo().begin() + i);
		}

	}

	for (int i = 0; i < unit->warcraft().size(); i++)
	{
		if (unit->warcraft()[i]->getHp() <= 0)
		{
			cout << unit->warcraft()[i]->getName() << " died" << endl;
			unit->warcraft().erase(unit->warcraft().begin() + i);
		}

	}
}

int main()
{
	srand(time(0));


	

		Unit* warcraft1 = new Unit("[Warcraft]", "E.T.C", "Multi");
		unit->addUnit(warcraft1);
		unit->addWarcraft(warcraft1);

		Unit* warcraft2 = new Unit("[Warcraft]", "Illidan", "Single");
		unit->addUnit(warcraft2);
		unit->addWarcraft(warcraft2);

		Unit* warcraft3 = new Unit("[Warcraft]", "LiLi", "Healer");
		unit->addUnit(warcraft3);
		unit->addWarcraft(warcraft3);

		Unit* diablo1 = new Unit("[Diablo]", "Johanna", "Multi");
		unit->addUnit(diablo1);
		unit->addDiablo(diablo1);

		Unit* diablo2 = new Unit("[Diablo]", "Valla", "Single");
		unit->addUnit(diablo2);
		unit->addDiablo(diablo2);

		Unit* diablo3 = new Unit("[Diablo]", "Kharazim", "Healer");
		unit->addUnit(diablo3);
		unit->addDiablo(diablo3);

		Action* baseAtk = new BasicAttack();
		Action* thehunt = new SingleTargetAttack();
		Action* facemelt = new MultiTargetAttack();
		Action* jugOfLife = new Heal();
		Action* puncturingArrow = new SingleTargetAttack();
		Action* divinePalm = new Heal();

		warcraft1->addAction(baseAtk);
		warcraft1->addAction(facemelt);

		warcraft2->addAction(baseAtk);
		warcraft2->addAction(thehunt);

		warcraft3->addAction(baseAtk);
		warcraft3->addAction(jugOfLife);

		diablo1->addAction(baseAtk);
		diablo1->addAction(facemelt);

		diablo2->addAction(baseAtk);
		diablo2->addAction(puncturingArrow);

		diablo3->addAction(baseAtk);
		diablo3->addAction(divinePalm);
	
	printTeams();

	unit->setTurnOrder();

	

	while (unit->diablo().size() > 0 && unit->warcraft().size() > 0)
	{
		int minMp1 = 3;
		int minMp2 = 4;
		int minMp3 = 5;

		system("pause");
		system("cls");

		cout << "Current turn: " << unit->getTurnOrder()[0]->getTeam() << unit->getTurnOrder()[0]->getName() << endl;

		for (int i = 0; i < unit->getTurnOrder().size(); i++)
		{
			unit->getTurnOrder()[i]->setHp();
			unit->getTurnOrder()[i]->setMp();
		}

		if (unit->getTurnOrder()[0]->getTeam() == "[Warcraft]")
		{
			unit->getTurnOrder()[0]->printStats();
			cout << "==========================" << endl;
		
			do
			{
				unit->chooseAction(unit->getTurnOrder()[0]);
			} while (unit->getTurnOrder()[0]->getMp() <= 2 && unit->getChooseAction() == 2);
		
			if (unit->getChooseAction() == 1)
			{
				unit->printDiablo();
				unit->chooseTarget();

				baseAtk->activate(unit->getTurnOrder()[0], unit->diablo()[unit->getTarget() - 1]);
			}
			else if (unit->getChooseAction() == 2 && unit->getTurnOrder()[0]->getClass() == "Single")
			{
				unit->printDiablo();
				unit->chooseTarget();
			
				thehunt->activate(unit->getTurnOrder()[0], unit->diablo()[unit->getTarget() - 1]);
			}
			else if (unit->getChooseAction() == 2 && unit->getTurnOrder()[0]->getClass() == "Multi")
			{
				for (int i = 0; i < unit->diablo().size(); i++)
				{
					facemelt->activate(unit->getTurnOrder()[0], unit->diablo()[i]);
				}
				unit->getTurnOrder()[0]->setMp();
				unit->getTurnOrder()[0]->subtractMp(4);

				cout << unit->getTurnOrder()[0]->getMp() << "/ " << unit->getTurnOrder()[0]->getMaxMp() << " Mp left" << endl;

			}
			else if (unit->getChooseAction() == 2 && unit->getTurnOrder()[0]->getClass() == "Healer")
			{
				unit->printWarcraft();

				unit->chooseTarget();
				
				jugOfLife->activate(unit->getTurnOrder()[0], unit->warcraft()[unit->getTarget() - 1]);
			}

		
		}
	

		else if (unit->getTurnOrder()[0]->getTeam() == "[Diablo]")
		{
		
			int randSkill = rand() % 2 + 1;
			int randChoose = rand() % unit->warcraft().size();
			int randChoose1 = rand() % unit->diablo().size();

			if (unit->getTurnOrder()[0]->getMp() >= minMp1)
			{
				randSkill = 1;
			}
		
			if (randSkill == 1)
			{
				baseAtk->activate(unit->getTurnOrder()[0], unit->warcraft()[randChoose]);
			}
			else if (randSkill == 2 && unit->getTurnOrder()[0]->getClass() == "Single" && unit->getTurnOrder()[0]->getMp() >= minMp3)
			{
			
		
				puncturingArrow->activate(unit->getTurnOrder()[0], unit->warcraft()[randChoose]);
			}
			else if (randSkill == 2 && unit->getTurnOrder()[0]->getClass() == "Multi" && unit->getTurnOrder()[0]->getMp() >= minMp2)
			{
				
				for (int i = 0; i < unit->warcraft().size(); i++)
				{
					facemelt->activate(unit->getTurnOrder()[0], unit->warcraft()[i]);
				}
				unit->getTurnOrder()[0]->setMp();
				unit->getTurnOrder()[0]->subtractMp(4);

				cout << unit->getTurnOrder()[0]->getMp() << "/ " << unit->getTurnOrder()[0]->getMaxMp() << " Mp left" << endl;
			}
			else if (randSkill == 2 && unit->getTurnOrder()[0]->getClass() == "Healer" && unit->getTurnOrder()[0]->getMp() >= minMp1)
			{
			
				divinePalm->activate(unit->getTurnOrder()[0], unit->diablo()[randChoose1]);

	
			}
			
			
		}

		system("pause");
		system("cls");



		unitCheck();

		turnRemove();


		cout << "==========================" << endl;
	

		printTeams();

		unit->displayTurnOrder();

		
	
	}
	system("pause");
	system("cls");


	if (unit->warcraft().size() <= 0)
	{
		cout << "Winner!" << endl;
		cout << "==========================" << endl;

		unit->printDiablo();

	}
	else if (unit->diablo().size() <= 0)
	{
		cout << "Winner!" << endl;
		cout << "==========================" << endl;

		unit->printWarcraft();
	}




	system("pause");
	return 0;
}