#include "Unit.h"
#include "Action.h"


Unit::Unit()
{
	mName = "default";
}

Unit::Unit(string team, string name, string type)
{

	mName = name;
	mTeam = team;
	mClass = type;
	mCurrHp = rand() % (30 - 20 + 1) + 20;
	mMaxHp = mCurrHp;
	mCurrMp = rand() % (20 - 10 + 1) + 10;
	mMaxMp = mCurrMp;
	mPow = rand() % (15 - 5 + 1) + 5;
	mVit = rand() % (10 - 1 + 1) + 1;
	mAgi = rand() % (10 - 1 + 1) + 1;
	mDex = rand() % (10 - 1 + 1) + 1;
}

Unit::~Unit()
{
	for (int i = 0; i < mTurnOrder.size(); i++)
	{
		delete mTurnOrder[i];
	}

	for (int i = 0; i < mDiablo.size(); i++)
	{
		delete mDiablo[i];
	}

	for (int i = 0; i < mWarcraft.size(); i++)
	{
		delete mWarcraft[i];
	}

	for (int i = 0; i < mAction.size(); i++)
	{
		delete mAction[i];
	}

}

string Unit::getName()
{
	return mName;
}

string Unit::getTeam()
{
	return mTeam;
}

string Unit::getClass()
{
	return mClass;
}

int Unit::getHp()
{
	return mCurrHp;
}

int Unit::getMaxHp()
{
	return mMaxHp;
}

int Unit::getMp()
{
	return mCurrMp;
}

int Unit::getMaxMp()
{
	return mMaxMp;
}

int Unit::getPow()
{
	return mPow;
}

float Unit::getVit()
{
	return mVit;
}

float Unit::getAgi()
{
	return mAgi;
}

int Unit::getDex()
{
	return mDex;
}


void Unit::printStats()
{
	cout << "Name: " << mName << endl;
	cout << "Hp: " << mCurrHp << "/ " << mMaxHp << endl;
	cout << "Mp: " << mCurrMp << "/ " << mMaxMp << endl;
	cout << "Power: " << mPow << endl;
	cout << "Vitality: " << mVit << endl;
	cout << "Agility: " << mAgi << endl;
	cout << "Dexteririty: " << mDex << endl;
}



void Unit::setHp()
{
	if (mCurrHp < 1)
	{
		mCurrHp = 0;
	}
	if (mCurrHp > mMaxHp)
	{
		mCurrHp = mMaxHp;
	}

}

void Unit::subtractHp(int value)
{
	mCurrHp -= value;
}

void Unit::addHp(int value)
{
	mCurrHp += value;
}

void Unit::setMp()
{
	if (mCurrMp < 1)
	{
		mCurrMp = 0;
	}
	if (mCurrMp > mMaxMp)
	{
		mCurrMp = mMaxMp;
	}


}

void Unit::subtractMp(int value)
{
	mCurrMp -= value;
}

void Unit::addMp(int value)
{
	mCurrMp += value;

}

vector<Unit*>& Unit::getTurnOrder()
{
	return mTurnOrder;
}

void Unit::addUnit(Unit* unit)
{
	mTurnOrder.push_back(unit);
}

void Unit::removeUnit()
{


}

void Unit::printDiablo()
{
	for (int i = 0; i < diablo().size(); i++)
	{
		cout << i + 1 << ". " << diablo()[i]->getTeam() << " " << diablo()[i]->getName() << " |  Hp: " << diablo()[i]->getHp();
		if (diablo()[i]->getHp() > 0)
		{
			cout << "  | [Alive]" << endl;
		}
		else if (diablo()[i]->getHp() <= 0)
		{
			cout << "  | [Dead]" << endl;

		}
	}
}

vector<Unit*>& Unit::diablo()
{
	return mDiablo;
}

void Unit::addDiablo(Unit* member)
{
	mDiablo.push_back(member);
}

void Unit::printWarcraft()
{
	for (int i = 0; i < warcraft().size(); i++)
	{
		cout << i + 1 << ". " << warcraft()[i]->getTeam() << " " << warcraft()[i]->getName() << " |  Hp: " << warcraft()[i]->getHp();
		if (warcraft()[i]->getHp() > 0)
		{
			cout << "  | [Alive]" << endl;
		}
		else if (warcraft()[i]->getHp() <= 0)
		{
			cout << "  | [Dead]" << endl;

		}
	}
}

vector<Unit*>& Unit::warcraft()
{
	return mWarcraft;
}

void Unit::addWarcraft(Unit* member)
{
	mWarcraft.push_back(member);
}

void Unit::setTurnOrder()
{


	for (int i = 0; i < getTurnOrder().size(); i++)
	{
		for (int j = 0; j < getTurnOrder().size(); j++)
		{
			if (mTurnOrder[i]->getAgi() > mTurnOrder[j]->getAgi())
			{
				swap(mTurnOrder[i], mTurnOrder[j]);

			}

		}

	}

	cout << "TURN ORDER" << endl;
	cout << "==========================" << endl;

	for (int i = 0; i < getTurnOrder().size(); i++)
	{

		cout << i + 1 << ". " << getTurnOrder()[i]->getTeam() << " " << getTurnOrder()[i]->getName() << endl;

	}

	cout << "==========================" << endl;

	cout << "Current Turn: " << getTurnOrder()[0]->getTeam() << " " << getTurnOrder()[0]->getName() << endl;
}

void Unit::displayTurnOrder()
{
	mTurnOrder.push_back(mTurnOrder[0]);
	mTurnOrder.erase(mTurnOrder.begin() + 0);


	cout << "TURN ORDER" << endl;
	cout << "==========================" << endl;

	for (int i = 0; i < getTurnOrder().size(); i++)
	{

		cout << i + 1 << ". " << getTurnOrder()[i]->getTeam() << " " << getTurnOrder()[i]->getName() << endl;

	}

	cout << "==========================" << endl;

	cout << "Current Turn: " << getTurnOrder()[0]->getTeam() << " " << getTurnOrder()[0]->getName() << endl;
}

int Unit::getTarget()
{
	return mChooseTarget;
}

void Unit::chooseTarget()
{


	cin >> mChooseTarget;


}

vector<Action*>& Unit::getAction()
{
	return mAction;
}

int Unit::getChooseAction()
{
	return mChooseAction;
}

void Unit::addAction(Action* skill)
{
	getAction().push_back(skill);
}

void Unit::chooseAction(Unit* actor)
{
	for (int i = 0; i < actor->getAction().size(); i++)
	{
		cout << i + 1 << ". " << actor->getAction()[i]->getName() << "  | Mp cost: " << actor->getAction()[i]->getMp() << endl;
	}

	cin >> mChooseAction;


}

int Unit::getHitRate()
{
	return mHitRate;
}

void Unit::setHitRate(Unit* attacker, Unit* target)
{
	int hitPercent = (attacker->getDex() / target->getAgi()) * 100;

	if (hitPercent > 80)
	{
		hitPercent = 80;
	}
	else if (hitPercent < 20)
	{
		hitPercent = 20;
	}

	int randValue = rand() % (hitPercent - 20 + 1) + 20;
	mHitRate = randValue;
}




