#pragma once
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Action;

class Unit
{
public:
	Unit();
	Unit(string team, string name, string type);
	~Unit();

	string getName();
	string getTeam();
	string getClass();
	int getHp();
	int getMaxHp();
	int getMp();
	int getMaxMp();
	int getPow();
	float getVit();
	float getAgi();
	int getDex();



	void printStats();
	void setHp();
	void subtractHp(int value);
	void addHp(int value);
	void setMp();
	void subtractMp(int value);
	void addMp(int value);

	vector<Unit*>& getTurnOrder();
	void addUnit(Unit* unit);
	void removeUnit();

	void printDiablo();
	vector<Unit*>& diablo();
	void addDiablo(Unit* member);

	void printWarcraft();
	vector<Unit*>& warcraft();
	void addWarcraft(Unit* member);

	void setTurnOrder();
	void displayTurnOrder();

	int getTarget();
	void chooseTarget();

	vector<Action*>& getAction();
	int getChooseAction();
	void addAction(Action* skill);
	void chooseAction(Unit* actor);

	int getHitRate();
	void setHitRate(Unit* attacker, Unit* target);
private:

	string mName;
	string mTeam;
	string mClass;
	int mCurrHp;
	int mMaxHp;
	int mCurrMp;
	int mMaxMp;
	int mPow;
	int mVit;
	float mAgi;
	float mDex;

	int mChooseTarget;
	int mChooseAction;
	int mHitRate;

	vector<Unit*> mTurnOrder;
	vector<Unit*> mDiablo;
	vector<Unit*> mWarcraft;
	vector<Action*> mAction;
};

