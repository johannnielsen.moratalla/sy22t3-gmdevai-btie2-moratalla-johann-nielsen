#include "Action.h"

Action::Action(string name, int mp)
{
    mName = name;
    mMp = mp;
}

string Action::getName()
{
    return mName;
}

int Action::getMp()
{
    return mMp;
}

void Action::setMp(int value)
{
    mMp = value;
}
