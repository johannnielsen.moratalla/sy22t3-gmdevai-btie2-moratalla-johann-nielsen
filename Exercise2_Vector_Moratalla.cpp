#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

vector<string> fillInventory()
{
    vector<string> inventory;
    string items[] = { "RedPotion","Elixir","EmptyBottle","BluePotion" };

    for (int i = 0; i < 10; i++)
    {
        int randomIndex = rand() % 4; //randomly select item from array
        inventory.push_back(items[randomIndex]);
    }
    return inventory;
}

void displayInventory(const vector<string>& inventory)
{
    for (const auto& item : inventory)
    {
        cout << item << endl;
    }
}

int countItem(const vector<string>& inventory, const string& item)
{
    int count = 0;

    for (const auto& currentItem : inventory)
    {
        if (currentItem == item)
        {
            count++;
        }
    }
    return count;
}

void removeItem(vector<string>& inventory, const string& item)
{
    inventory.erase
    (remove_if(inventory.begin(), inventory.end(),
        [&](const string& currentItem)
            {
                return currentItem == item;
            }
        ),inventory.end());
}

int main()
{
    vector<string> inventory = fillInventory();
    cout << "Inventory: " << endl;
    cout << "====================" << endl;
    displayInventory(inventory);

    cout << "Number of RedPotion instances: " << countItem(inventory, "RedPotion") << endl;
    cout << "========================================" << endl;

    removeItem(inventory, "Elixir"); // it says specific not random so i choose Elixir

    cout << "Inventory after removing Elixir:" << endl;
    displayInventory(inventory);

    return 0;
}