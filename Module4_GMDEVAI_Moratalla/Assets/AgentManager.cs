﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentManager : MonoBehaviour
{
    GameObject[] agents;
    public GameObject player;
    public float stoppingDistance = 2f;
    // Start is called before the first frame update
    void Start()
    {
        agents = GameObject.FindGameObjectsWithTag("AI");
    }

    // Update is called once per frame
    void Update()
    //{
    //    if(Input.GetMouseButton(0)) 
    //    {
    //        RaycastHit hit;

    //        if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000))
    //        {
    //            foreach (GameObject ai  in agents) 
    //            {
    //                ai.GetComponent<AIControl>().agent.SetDestination(hit.point);
    //            }
    //        }
    //    }
    //}
    {
        if (player == null)
        {
            Debug.LogError("Player GameObject is not assigned to AgentManager!");
            return;
        }

        foreach (GameObject ai in agents)
        {
            float distanceToPlayer = Vector3.Distance(ai.transform.position, player.transform.position);

            if (distanceToPlayer > stoppingDistance)
            {
                ai.GetComponent<AIControl>().agent.SetDestination(player.transform.position);
            }
            else
            {
                ai.GetComponent<AIControl>().agent.SetDestination(ai.transform.position);
            }
        }
    }
}
