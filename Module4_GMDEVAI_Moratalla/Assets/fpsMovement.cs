﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fpsMovement : MonoBehaviour
{
    public float movementSpeed = 5f;
    public float mouseSensitivity = 3f;

    private CharacterController characterController;
    private Camera playerCamera;
    private float verticalRotation = 0f;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        characterController = GetComponent<CharacterController>();
        playerCamera = GetComponentInChildren<Camera>();
    }

    void Update()
    {
        // Mouse input for looking around
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity;

        verticalRotation -= mouseY;
        verticalRotation = Mathf.Clamp(verticalRotation, -90f, 90f);

        playerCamera.transform.localRotation = Quaternion.Euler(verticalRotation, 0f, 0f);
        transform.Rotate(Vector3.up * mouseX);

        // Keyboard input for movement
        float moveForwardBackward = Input.GetAxis("Vertical") * movementSpeed;
        float moveLeftRight = Input.GetAxis("Horizontal") * movementSpeed;

        Vector3 movement = transform.forward * moveForwardBackward + transform.right * moveLeftRight;
        characterController.SimpleMove(movement);
    }
}
