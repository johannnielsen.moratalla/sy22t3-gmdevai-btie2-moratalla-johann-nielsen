using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIControl : MonoBehaviour
{
    NavMeshAgent agent;

    public GameObject target;

    public WASDMovement playerMovement;

    Vector3 wanderTarget;

    public float distanceToAgent = 10f;


    // Start is called before the first frame update
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        playerMovement = target.GetComponent<WASDMovement>();
    }

    void Seek(Vector3 location)
    {
        agent.SetDestination(location);
    }

    void Flee(Vector3 location) 
    {
        Vector3 fleeDirection = location - this.transform.position;
        agent.SetDestination(this.transform.position - fleeDirection);
    }

    void Pursue()
    {
        Vector3 targetDirection = target.transform.position - this.transform.position;

        float lookAhead = targetDirection.magnitude/(agent.speed + playerMovement.currentSpeed);

        Seek(target.transform.position + target.transform.forward*lookAhead);
    }

    void Evade()
    {
        Vector3 targetDirection = target.transform.position - this.transform.position;

        float lookAhead = targetDirection.magnitude / (agent.speed + playerMovement.currentSpeed);

        Flee(target.transform.position + target.transform.forward * lookAhead);
    }

    
    float Wander()
    {
        
        float wanderRadius = 20;
        float wanderDistance = 10;
        float wanderJitter = 1;

        distanceToAgent = 15f;
        

        wanderTarget += new Vector3(Random.Range(-1.0f, 1.0f) * wanderJitter,
                                    0, 
                                    Random.Range(-1.0f, 1.0f) * wanderJitter);
        wanderTarget.Normalize();
        wanderTarget *= wanderRadius;

        Vector3 targetLocal = wanderTarget + new Vector3(0, 0, wanderDistance);
        Vector3 targetWorld = this.gameObject.transform.InverseTransformVector(targetLocal);

        Seek(targetWorld);

        float distanceToTarget = Vector3.Distance(this.gameObject.transform.position, targetWorld);

        return distanceToTarget;


    }


    
    void Hide()
    {
        float distance = Mathf.Infinity;
        Vector3 chosenSpot = Vector3.zero;

        int hidingSportsCount = World.Instance.GetHidingSpots().Length;

        for(int i = 0; i < hidingSportsCount; i++)
        {
            Vector3 hideDirection = World.Instance.GetHidingSpots()[i].transform.position - target.transform.position;
            Vector3 hidePosition = World.Instance.GetHidingSpots()[i].transform.position + hideDirection.normalized * 5; 

            float spotDistance = Vector3.Distance(this.transform.position, hidePosition);
            if(spotDistance < distance)
            {
                chosenSpot = hidePosition;
                distance = spotDistance;
            }
        }

        Seek(chosenSpot);
    }
    void CleverHide()
    {
        float distance = Mathf.Infinity;
        Vector3 chosenSpot = Vector3.zero;
        Vector3 chosenDir = Vector3.zero;
        GameObject chosenGameObject = World.Instance.GetHidingSpots()[0];

        int hidingSportsCount = World.Instance.GetHidingSpots().Length;

        for (int i = 0; i < hidingSportsCount; i++)
        {
            Vector3 hideDirection = World.Instance.GetHidingSpots()[i].transform.position - target.transform.position;
            Vector3 hidePosition = World.Instance.GetHidingSpots()[i].transform.position + hideDirection.normalized * 5;

            float spotDistance = Vector3.Distance(this.transform.position, hidePosition);
            if (spotDistance < distance)
            {
                chosenSpot = hidePosition;
                chosenDir = hideDirection;
                chosenGameObject = World.Instance.GetHidingSpots()[i];
                distance = spotDistance;
            }
        }

        Collider hideCol = chosenGameObject.GetComponent<Collider>();
        Ray back = new Ray(chosenSpot, -chosenDir.normalized);
        RaycastHit info;
        float rayDsitance = 100.0f;
        hideCol.Raycast(back, out info, rayDsitance);

        Seek(info.point + chosenDir.normalized * 5);
    }

    public enum AIBehaviorType
    {
        wanderPursue,
        wanderHides,
        wanderEvades,
    }

    public List<AIBehaviorType> behaviors = new List<AIBehaviorType>();
    private int currentBehaviorIndex = 0;

    //bool canSeeTarget()
    //{
    //    RaycastHit raycastInfo;
    //    Vector3 rayToTarget = target.transform.position - this.transform.position;

    //    if (Physics.Raycast(this.transform.position, rayToTarget, out raycastInfo)) 
    //    {
    //        return raycastInfo.transform.gameObject.tag == "Player";
    //    }
    //    return false;
    //}

    

    // Update is called once per frame
    void Update()
    {
        

        float distanceToPlayer = Vector3.Distance(transform.position, target.transform.position);
        
        
            AIBehaviorType currentBehavior = behaviors[currentBehaviorIndex];

            switch (currentBehavior)
            {
                case AIBehaviorType.wanderPursue:
                    if (distanceToAgent >=distanceToPlayer)
                    {
                        Pursue();
                    }
                    else
                    {
                        Wander();
                    }
                    break;
                case AIBehaviorType.wanderHides:
                    if (distanceToAgent >= distanceToPlayer)
                {
                        Hide();
                    }
                    else
                    {
                        Wander();
                    }
                    break;
                case AIBehaviorType.wanderEvades:
                    if (distanceToAgent >= distanceToPlayer)
                {
                        Evade();
                    }
                    else
                    {
                        Wander();
                    }
                    break;
                default:
                    Debug.LogError("Unknown AI Behavior type: " + currentBehavior);
                    break;
            }
        
        //if(canSeeTarget())
        //{   
        //}

        
        Debug.Log("Distance from player to agent: " + distanceToPlayer);
    }
}
