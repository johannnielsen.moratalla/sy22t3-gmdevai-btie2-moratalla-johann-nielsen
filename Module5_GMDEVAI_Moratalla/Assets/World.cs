using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class World
{
   private static readonly World insntance = new World();

    private static GameObject[] hidingSpots;

    static World()
    {
        hidingSpots = GameObject.FindGameObjectsWithTag("Hide");
    }

    private World()
    {

    }

    public static World Instance
    {
        get { return insntance; }
    }

    public GameObject[] GetHidingSpots()
    {
        return hidingSpots;
    }
}
