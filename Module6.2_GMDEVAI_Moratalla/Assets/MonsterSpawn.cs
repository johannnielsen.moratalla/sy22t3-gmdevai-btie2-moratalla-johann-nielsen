using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawn : MonoBehaviour
{
    public GameObject obstacle;
    public GameObject crowdFlockTarget;

    GameObject[] agents;

    // Start is called before the first frame update
    void Start()
    {
        agents = GameObject.FindGameObjectsWithTag("agent");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            PlaceObstacle();
        }

        if (Input.GetMouseButtonDown(1))
        {
            PlaceFlockTarget();
        }
    }

    void PlaceObstacle()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray.origin, ray.direction, out hit))
        {
            Instantiate(obstacle, hit.point, obstacle.transform.rotation);
            foreach (GameObject a in agents)
            {
                a.GetComponent<AIControl>().DetectNewObstacle(hit.point);
            }
        }
    }

    void PlaceFlockTarget()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray.origin, ray.direction, out hit))
        {
            Instantiate(crowdFlockTarget, hit.point, crowdFlockTarget.transform.rotation);
            foreach (GameObject a in agents)
            {
                a.GetComponent<AIControl>().SetFlockTarget(hit.point);
            }
        }
    }
}
