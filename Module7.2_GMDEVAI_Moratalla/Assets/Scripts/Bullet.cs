﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

	public GameObject explosion;

	public int damageAmount = 10;

	
	void OnCollisionEnter(Collision col)
    {
    	
    }

    void OnTriggerEnter(Collider other)
    {

        GameObject e = Instantiate(explosion, this.transform.position, Quaternion.identity);
        Destroy(e, 1.5f);
        Destroy(this.gameObject);

        
       
            HP playerShooting = other.GetComponent<HP>();
            if (playerShooting != null)
            {
                playerShooting.TakeDamage(damageAmount);
            }

            Destroy(gameObject);
        
    }
}
