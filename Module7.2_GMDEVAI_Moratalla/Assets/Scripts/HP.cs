using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class HP : MonoBehaviour
{
    Animator animator;

    public int maxHealth = 100;

    [SerializeField] private int currentHealth;

    [SerializeField] private Healthbar healthBar;



    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();

        currentHealth = maxHealth;

        healthBar.updateHealthbar(maxHealth, currentHealth);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TakeDamage(int damageAmount)
    {
        currentHealth -= damageAmount;
        healthBar.updateHealthbar(maxHealth, currentHealth);
        if (currentHealth <= 0 )
        {
            Die();
        }
        if (currentHealth <= 20)
        {
            animator.SetBool("isLow", true);
            
        }
    }

    void Die()
    {
        TankAI tankAI = GetComponent<TankAI>();
        if (tankAI != null)
        {
            tankAI.StopFiring();
        }
        gameObject.SetActive(false);
    }
}
