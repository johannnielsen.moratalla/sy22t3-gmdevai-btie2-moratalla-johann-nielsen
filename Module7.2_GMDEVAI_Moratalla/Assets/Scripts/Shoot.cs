using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public GameObject bullet;
    public Transform turret;
    public float shootForce = 10f;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            fire();
        }
    }

    void fire()
    {
        GameObject projectile = Instantiate(bullet, turret.position, turret.rotation);

        Rigidbody projectileRigidbody = projectile.GetComponent<Rigidbody>();
        if (projectileRigidbody != null)
        {
            projectileRigidbody.AddForce(turret.forward * shootForce, ForceMode.Impulse);
        }
    }
}
