using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.FilePathAttribute;

public class TankAI : MonoBehaviour
{
    Animator anim;

    public GameObject player;

    public GameObject bullet;
    public GameObject turret;

    

    public GameObject GetPlayer()
    {
        return player;
    }
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetFloat("distance", Vector3.Distance(transform.position, player.transform.position));
    }

    void fire()
    {
        GameObject b =Instantiate(bullet, turret.transform.position, turret.transform.rotation);
        b.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 500);
    }

    public void StopFiring()
    {
        CancelInvoke("fire");
    }

    public void StartFiring()
    {
        InvokeRepeating("fire", 0.5f, 0.5f);
    }

    public void Flee()
    {

     // if (currentHealth <= 20)
        {
            anim.SetBool("isLow", true);
        }
    }
}
